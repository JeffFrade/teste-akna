CREATE DATABASE `teste-akna`;
USE `teste-akna`;

CREATE TABLE products(
    id INT PRIMARY KEY AUTO_INCREMENT,
    month VARCHAR(25) NOT NULL,
    category VARCHAR(35) NOT NULL,
    product VARCHAR(100) NOT NULL,
    quantity INT NOT NULL
);
