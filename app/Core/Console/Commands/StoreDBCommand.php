<?php

namespace App\Core\Console\Commands;

use App\Services\ShoppingList;
use Illuminate\Console\Command;

class StoreDBCommand extends Command
{
    protected $signature = 'store:db';

    protected $description = 'Store data from list in DB';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ShoppingList $shoppingList)
    {
        $values = $shoppingList->storeDb();

        if ($values['success']) {
            $this->info($values['message']);
            return true;
        }

        $this->error($values['message']);
        return false;
    }
}