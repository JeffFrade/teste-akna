<?php

namespace App\Core\Console\Commands;

use App\Services\ShoppingList;
use Illuminate\Console\Command;

class GenerateCsvCommand extends Command
{
    protected $signature = 'generate:csv';

    protected $description = 'Generate a CSV from list';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ShoppingList $shoppingList)
    {
        $values = $shoppingList->generateCsv();

        if ($values['success']) {
            $this->info($values['message']);
            return true;
        }

        $this->error($values['message']);
        return false;
    }
}