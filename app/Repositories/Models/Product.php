<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'month',
        'category',
        'product',
        'quantity'
    ];
}