<?php


namespace App\Services;


use Maatwebsite\Excel\Concerns\FromArray;

class ExportList implements FromArray
{
    protected $list;

    public function __construct(array $list)
    {
        $this->list = $list;
    }

    public function array(): array
    {
        return $this->list;
    }
}
