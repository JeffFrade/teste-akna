<?php

namespace App\Services;

use App\Helpers\DateHelper;
use App\Helpers\StringHelper;
use App\Repositories\ProductRepository;
use Maatwebsite\Excel\Facades\Excel;

class ShoppingList
{
    private $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    public function generateCsv()
    {
        try {
            $csvContent = [['Mês', 'Categoria', 'Produto', 'Quantidade']];
            $list = config('lista-de-compras');

            foreach ($list as $monthItem => $monthList) {
                foreach ($monthList as $typeItem => $typeList) {
                    foreach ($typeList as $item => $value) {
                        $csvContent[] = [
                            $monthItem,
                            $typeItem,
                            StringHelper::fixWords($item),
                            $value
                        ];
                    }
                }
            }

            $csvContent = DateHelper::sortMonths($csvContent);
            $filename = 'list.csv';

            Excel::store(new ExportList($csvContent), $filename, 'public');

            return [
                'success' => true,
                'message' => 'Arquivo Gerado com Êxito em: ' . $filename
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => 'Erro: ' . $e->getCode() . ' - ' . $e->getMessage()
            ];
        }
    }

    public function storeDb()
    {
        try {
            $list = config('lista-de-compras');

            foreach ($list as $monthItem => $monthList) {
                foreach ($monthList as $typeItem => $typeList) {
                    foreach ($typeList as $item => $value) {
                        $data = [
                            'month' => $monthItem,
                            'category' => $typeItem,
                            'product' => StringHelper::fixWords($item),
                            'quantity' => $value
                        ];

                        $this->productRepository->create($data);
                    }
                }
            }

            return [
                'success' => true,
                'message' => 'Dados Inseridos no Banco de Dados com Sucesso!'
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'message' => 'Erro: ' . $e->getCode() . ' - ' . $e->getMessage()
            ];
        }
    }
}
