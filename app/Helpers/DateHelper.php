<?php

namespace App\Helpers;

class DateHelper
{
    public static function sortMonths(array $csv) {
        $months = [
            'janeiro' => 0,
            'fevereiro' => 1,
            'marco' => 2,
            'abril' => 3,
            'maio' => 4,
            'junho' => 5,
            'julho' => 6,
            'agosto' => 7,
            'setembro' => 8,
            'outubro' => 9,
            'novembro' => 10,
            'dezembro' => 11,
        ];

        foreach ($csv as $i => $c) {
            if ($i >= 1) {
                $csv[$i][0] = $months[$c[0]];
            }
        }

        asort($csv);

        $months = array_keys($months);

        foreach ($csv as $i => $c) {
            if ($i >= 1) {
                $csv[$i][0] = $months[$c[0]];
            }
        }

        return $csv;
    }
}
