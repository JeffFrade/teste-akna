<?php

namespace App\Helpers;

class StringHelper
{
    public static function fixWords($text = '')
    {
        $patterns = [
            '/Papel\s+Hignico/i',
            '/Brocolis/i',
            '/Chocolate\s+ao\s+leit/i',
            '/Sabao\s+em\s+po/i',
        ];

        $replaces = [
            'Papel Higiênico',
            'Brócolis',
            'Chocolate ao leite',
            'Sabão em pó',
        ];

        return preg_replace($patterns, $replaces, $text);
    }
}
